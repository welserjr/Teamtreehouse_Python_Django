from django.conf.urls import url

from .views import course_list, course_detail, step_detail

urlpatterns = [
    url(r'^$', course_list, name='list'),
    url(r'(?P<course_pk>\d+)/(?P<step_pk>\d+)/$', step_detail,
        name='step'),
    url(r'(?P<pk>\d+)/$', course_detail, name='detail'),
]
