from datetime import date
from django.contrib import admin
from .models import Text, Quiz, Answer, Course, MultipleChoiceQuestion, \
    TrueFalseQuestion


def make_published(modeladmin, request, queryset):
    queryset.update(status='p', published=True)
make_published.short_description = 'Mark selected courses as Published'


def make_in_review(modeladmin, request, queryset):
    queryset.update(status='r', published=False)
make_in_review.short_description = 'Mark selected courses as In Progress'


def make_in_progress(modeladmin, request, queryset):
    queryset.update(status='i', published=False)
make_in_progress.short_description = 'Mark selected courses as In Review'

admin.site.disable_action('delete_selected')


class TextInline(admin.StackedInline):
    model = Text
    fieldsets = (
        (None, {
            'fields': (('title', 'order'), 'description', 'content')
        }),
    )


class QuizInLine(admin.StackedInline):
    model = Quiz


class AnswerInLine(admin.TabularInline):
    model = Answer


class YearListFilter(admin.SimpleListFilter):
    title = 'year created'
    parameter_name = 'year'

    def lookups(self, request, model_admin):
        return (
            ('2015', '2015'),
            ('2016', '2016'),
        )

    def queryset(self, request, queryset):
        if self.value() == '2015':
            return queryset.filter(created_at__gte=date(2015, 1, 1),
                                   created_at__lte=date(2015, 12, 31))
        if self.value() == '2016':
            return queryset.filter(created_at__gte=date(2016, 1, 1),
                                   created_at__lte=date(2016, 12, 31))


class TopicListFilter(admin.SimpleListFilter):
    title = 'topic'
    parameter_name = 'topic'

    def lookups(self, request, model_admin):
        return (
            ('python', 'Python'),
            ('ruby', 'Ruby'),
            ('java', 'Java')
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(title__contains=self.value())


class CourseAdmin(admin.ModelAdmin):
    inlines = [TextInline, QuizInLine]
    list_filter = ['created_at', 'published', YearListFilter, TopicListFilter]
    list_display = ['title', 'created_at', 'published', 'time_to_complete', 'status']
    list_editable = ['status']
    search_fields = ['title', 'description']
    actions = [make_published]

    class Media:
        js = ('js/vendor/markdown.js', 'js/preview.js')
        css = {'all': ('css/preview.css', ), }


class QuestionAdmin(admin.ModelAdmin):
    inlines = [AnswerInLine, ]
    search_fields = ['prompt']
    list_display = ['prompt', 'quiz', 'order']
    list_editable = ['quiz', 'order']
    radio_fields = {'quiz': admin.HORIZONTAL}


class QuizAdmin(admin.ModelAdmin):
    fields = ['course', 'title', 'description', 'order', 'total_questions']
    search_fields = ['title', 'description']
    list_filter = ['course']
    list_display = ['title', 'course', 'total_questions']
    list_editable = ['course', 'total_questions']


class TextAdmin(admin.ModelAdmin):
    # fields = ['course', 'title', 'order', 'description', 'content']
    fieldsets = (
        (None, {
            'fields': ('course', 'title', 'order', 'description')}),
        ('Add content',
         {'fields': ('content', ),
          'classes': ('collapse', )})
    )


admin.site.register(Course, CourseAdmin)
admin.site.register(Text, TextAdmin)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(MultipleChoiceQuestion, QuestionAdmin)
admin.site.register(TrueFalseQuestion, QuestionAdmin)
admin.site.register(Answer)
