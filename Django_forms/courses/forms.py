from django import forms
from .models import Quiz, TrueFalseQuestion, \
    MultipleChoiceQuestion, Answer, Question


class QuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = [
            'title', 'description',
            'order', 'total_questions'
        ]


class QuestionForm(forms.ModelForm):
    class Media:
        css = {'all': ('courses/css/order.css', )}
        js = (
            'courses/js/vendor/jquery.fn.sortable.min.js',
            'courses/js/order.js'
        )


class TrueFalseQuestionForm(QuestionForm):
    class Meta:
        model = TrueFalseQuestion
        fields = ['order', 'prompt']


class MultipleChoiceQuestionForm(QuestionForm):
    class Meta:
        model = MultipleChoiceQuestion
        fields = ['order', 'prompt', 'shuffle_answers']


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = [
            'order', 'text', 'correct'
        ]

AnswerFormSet = forms.modelformset_factory(
    Answer,
    form=AnswerForm,
    extra=2
)

AnswerInlineFormSet = forms.inlineformset_factory(
    Question,
    Answer,
    extra=2,
    fields=('order', 'text', 'correct'),
    formset=AnswerFormSet,
    min_num=1
)
