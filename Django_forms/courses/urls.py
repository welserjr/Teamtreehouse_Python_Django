from django.conf.urls import url

from .views import course_list, text_detail, \
    quiz_detail, course_detail, quiz_create, \
    quiz_edit, create_question, edit_question, \
    answer_form

urlpatterns = [
    url(r'^$', course_list, name='list'),
    url(r'(?P<course_pk>\d+)/t(?P<step_pk>\d+)/$', text_detail,
        name='text'),
    url(r'(?P<course_pk>\d+)/q(?P<step_pk>\d+)/$', quiz_detail,
        name='quiz'),
    url(r'(?P<course_pk>\d+)/create_quiz/$', quiz_create,
        name='create_quiz'),
    url(r'(?P<course_pk>\d+)/edit_quiz/(?P<quiz_pk>\d+)$', quiz_edit,
        name='edit_quiz'),
    url(r'(?P<quiz_pk>\d+)/create_question/(?P<question_type>mc|tf)/$',
        create_question, name='create_question'),
    url(r'(?P<quiz_pk>\d+)/edit_question/(?P<question_pk>\d+)$', edit_question,
        name='edit_question'),
    url(r'(?P<question_pk>\d+)/create_answer/$', answer_form,
        name='create_answer'),
    url(r'(?P<pk>\d+)/$', course_detail, name='detail'),
]
