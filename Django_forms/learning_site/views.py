from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import SuggestionForm


def hello_world(request):
    return render(request, 'home.html')


def suggestion_view(request):
    form = SuggestionForm()
    if request.method == 'POST':
        form = SuggestionForm(request.POST)
        if form.is_valid():
            send_mail(
                'Suggestion from {}'.format(form.cleaned_data['name']),
                form.cleaned_data['email'],
                '{name} <{email}>'.format(**form.cleaned_data),
                ['welser.m.r@gmail.com']
            )
            messages.add_message(
                request,
                messages.SUCCESS,
                'Thanks for your suggestion!'
            )
            return HttpResponseRedirect(reverse('suggestion'))
    return render(request, 'suggestion_form.html', {'form': form})
