__author__ = 'welserjr'

import re

# names_file = open("names.txt", encoding="utf-8")
# data = names_file.read()
# names_file.close()
# print(data)

with open("names.txt", mode="r", encoding="utf-8") as names_file:
    data = names_file.read()
# print(data)

# print(re.match(r'Love', data))
# print(re.search(r'Kenneth', data))

last_name = r'Love'
# first_name = r'Kenneth'
first_name = r'Darth'
# print(re.match(last_name, data))
# print(re.search(first_name, data))

# print(re.match(r'\w\w\w\w, \w', data))
# print(re.search(r'\d\d\d-\d\d\d\d', data))
# print(re.search(r'\(\d\d\d\) \d\d\d-\d\d\d\d', data))

# print(re.search(r'\w+, \w+', data))
# print(re.match(r'\w+, \w+', data))

# print(re.search(r'\(\d{3}\) \d{3}-\d{4}', data))
# print(re.search(r'\(?\d{3}\)? \d{3}-\d{4}', data))
# print(re.findall(r'\(\d{3}\) \d{3}-\d{4}', data))
# print(re.findall(r'\(?\d{3}\)? \d{3}-\d{4}', data))
# print(re.findall(r'\(?\d{3}\)?-?\s?\d{3}-\d{4}', data))

# print(re.findall(r'\w+, \w+', data))
# print(re.findall(r'\w*, \w+', data))

# print(re.findall(r'[-\w\d+.]+@[-\w\d.]+', data))
# print(re.findall(r'[trehous]+', data, re.I))
# print(re.findall(r'\b[trehous]+\b', data, re.I))
# print(re.findall(r'\b[trehous]{9}\b', data, re.I))

# print(re.findall(r'''
#     \b@[-\w\d.]* # First a word boundary, an @, and then any number of characters
#     [^gov\t]+ # Ignore 1+ instances of a letter 'g', 'o' or 'v' and a tab
# ''', data, re.VERBOSE|re.I))

# print(re.findall(r'''
#    \b[-\w]+, # Find a boundary, 1+ hyphens or characters, and a comma
#    \s # Find 1 whitespace
#    [-\w ]+ # 1+ hyphens characters and explicit spaces
#    [^\t\n] # Ignore tabs and newlines
# ''', data, re.X))

# print(re.findall(r'''
#    ([-\w ]+,\s[-\w ]+)\t # Last and first names
#    ([-\w\d.+]+@[-\w\d.]+)\t # Email
#    (\(?\d{3}\)?-?\s?\d{3}-\d{4})\t # Phone
#    ([\w\s]+,\s[\w\s]+)\t # Job and company
#    (@[\w\d]+) # Twitter
# ''', data, re.X))

# print(re.findall(r'''
#    ^([-\w ]*,\s[-\w ]+)\t # Last and first names
#    ([-\w\d.+]+@[-\w\d.]+)\t # Email
#    (\(?\d{3}\)?-?\s?\d{3}-\d{4})?\t # Phone
#    ([\w\s]+,\s[\w\s.]+)\t? # Job and company
#    (@[\w\d]+)?$ # Twitter
# ''', data, re.X|re.M))

# line = re.search(r'''
#     ^(?P<name>[-\w ]*,\s[-\w ]+)\t # Last and first names
#     (?P<email>[-\w\d.+]+@[-\w\d.]+)\t # Email
#     (?P<phone>\(?\d{3}\)?-?\s?\d{3}-\d{4})?\t # Phone
#     (?P<job>[\w\s]+,\s[\w\s.]+)\t? # Job and company
#     (?P<twitter>@[\w\d]+)?$ # Twitter
# ''', data, re.X | re.M)
#
# print(line)
# print(line.groupdict())

line = re.compile(r'''
    ^(?P<name>(?P<last>[-\w ]*),\s(?P<first>[-\w ]+))\t # Last and first names
    (?P<email>[-\w\d.+]+@[-\w\d.]+)\t # Email
    (?P<phone>\(?\d{3}\)?-?\s?\d{3}-\d{4})?\t # Phone
    (?P<job>[\w\s]+,\s[\w\s.]+)\t? # Job and company
    (?P<twitter>@[\w\d]+)?$ # Twitter
''', re.X | re.M)

# print(re.search(line, data).groupdict())
# print(line.search(data).groupdict())

for match in line.finditer(data):
    #print(match.group('name'))
    print('{first} {last} <{email}>'.format(**match.groupdict()))
